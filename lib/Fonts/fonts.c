#include "fonts.h"


const uint8_t c_chBmp4016[96] =  //SUN
{
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3F,0xF1,0x81,0x8F,0xFC,0x3F,
  0xF1,0x81,0x8F,0xFC,0x30,0x31,0x81,0x8C,0x0C,0x30,0x01,0x81,0x8C,0x0C,0x30,0x01,
  0x81,0x8C,0x0C,0x3F,0xF1,0x81,0x8C,0x0C,0x3F,0xF1,0x81,0x8C,0x0C,0x00,0x31,0x81,
  0x8C,0x0C,0x00,0x31,0x81,0x8C,0x0C,0x30,0x31,0x81,0x8C,0x0C,0x3F,0xF1,0xFF,0x8C,
  0x0C,0x3F,0xF1,0xFF,0x8C,0x0C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

const uint8_t c_chSignal816[16] = //mobile signal
{
  0xFE,0x02,0x92,0x0A,0x54,0x2A,0x38,0xAA,0x12,0xAA,0x12,0xAA,0x12,0xAA,0x12,0xAA
};

const uint8_t c_chMsg816[16] =  //message
{
  0x1F,0xF8,0x10,0x08,0x18,0x18,0x14,0x28,0x13,0xC8,0x10,0x08,0x10,0x08,0x1F,0xF8
};

const uint8_t c_chBat816[16] = //battery
{
  0x0F,0xFE,0x30,0x02,0x26,0xDA,0x26,0xDA,0x26,0xDA,0x26,0xDA,0x30,0x02,0x0F,0xFE
};

const uint8_t c_chBluetooth88[8] = // bluetooth
{
  0x18,0x54,0x32,0x1C,0x1C,0x32,0x54,0x18
};

const uint8_t c_chGPRS88[8] = //GPRS
{
  0xC3,0x99,0x24,0x20,0x2C,0x24,0x99,0xC3
};

const uint8_t c_chAlarm88[8] = //alarm
{
  0xC3,0xBD,0x42,0x52,0x4E,0x42,0x3C,0xC3
};

