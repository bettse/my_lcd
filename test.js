const LCD = require(".");
const { Colors, HEIGHT, WIDTH } = LCD;
const { RED, BLACK, WHITE, GREEN, BLUE, GRAY } = Colors;

const x = WIDTH / 2;
const y = HEIGHT / 2;
const fontSize = 24;
const r = 20;
const fill = 1;
const color = RED;

LCD.init(); // TODO: support passing a backgorund color that all clear() calls will respect
LCD.clear();

LCD.drawString(0, 0, "hello");
LCD.drawCircle(x, y, r, color, fill);
LCD.drawString(0, y + fontSize, "world", fontSize, GREEN);
LCD.drawString(0, y + fontSize * 2, "invert", fontSize, WHITE, BLACK);
LCD.drawRect(LCD.WIDTH/2, 1, LCD.WIDTH, LCD.HEIGHT/2, GRAY);

setTimeout(() => {
  // Reset the LCD and set the draw mode to manual
  LCD.exit();
  LCD.init(1);

  LCD.clear(BLACK);
  LCD.drawString(0, 0, "hello", 12, WHITE, BLACK);
  LCD.drawCircle(x, y, r, color, fill);
  LCD.drawCircle(x, y, r + 4, BLUE, 0);
  LCD.drawString(0, y + fontSize, "world", fontSize, GREEN, BLACK);
  LCD.drawString(0, y + fontSize * 2, "invert", fontSize, BLACK, WHITE);
  LCD.draw(); // manually call draw to update screen

  // Draw series of things all at once
  LCD.draw(() => {
    LCD.clear(BLACK);
    LCD.drawString(0, 0, "hello", 12, RED, BLACK);
    LCD.drawCircle(x, y, r, BLUE, fill);
    LCD.drawCircle(x, y, r + 4, BLUE, 0);
    LCD.drawString(0, y + fontSize, "world", fontSize, GREEN, BLACK);
    LCD.drawString(0, y + fontSize * 2, "invert", fontSize, BLACK, WHITE);
  });

  LCD.exit();
}, 3000);
